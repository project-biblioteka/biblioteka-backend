###################  Docker development helpful directives  ####################
#
# Usage:
# make run-local             	# deploys the dev environment in your local machine in a venv
# make stop-local            	# stops the local dev environment
# make generate-lock					# also, it generates the Pipfile.lock
# make docker-build-env				# build and runs the backend in a Docker container
# make docker-rebuild-env			# force the image rebuilding and runs the backend in a Docker container
#

run-local:
	python3 -m venv venv-biblioteka-frontend
	. venv-biblioteka-frontend/bin/activate && pipenv install && flask run
.PHONY: run-local

stop-local:
	-deactivate
	rm -rf venv-biblioteka-frontend
.PHONY: stop-local

generate-lock:
	python3 -m venv venv-biblioteka-frontend
	. venv-biblioteka-frontend/bin/activate && pipenv install
	-deactivate
	rm -rf venv-biblioteka-frontend
.PHONY: generate-lock

docker-build-env:
	COMPOSE_PROJECT_NAME=biblioteka COMPOSE_HTTP_TIMEOUT=200 docker-compose -f docker-compose.yml up --force-recreate
.PHONY: docker-build-env

docker-rebuild-env:
	COMPOSE_PROJECT_NAME=biblioteka COMPOSE_HTTP_TIMEOUT=200 docker-compose -f docker-compose.yml up --force-recreate --build
.PHONY: docker-rebuild-env

docker-clean-env:
	COMPOSE_PROJECT_NAME=biblioteka docker-compose rm -v
.PHONY: docker-clean-env

docker-initdb:
	docker exec -it biblioteka-postgresql /bin/sh -c "/docker-entrypoint-initdb.d/initdb.sql"
.PHONY: docker-initdb
