from .utils import db
from marshmallow import Schema, fields, validate

class InternalNotes(db.Model):
    __tablename__ = 'internal_notes'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    note_id = db.Column(db.String(22))
    type = db.Column(db.SmallInteger())
    visibility = db.Column(db.SmallInteger())
    description = db.Column(db.Text())
    registered_on = db.Column(db.DateTime())
    size = db.Column(db.Numeric())
    owner = db.Column(db.String(100), db.ForeignKey('login_user.login'))
    content = db.relationship("NotesContent", uselist=False, cascade="all,delete")

class NotesContent(db.Model):
    __tablename__ = 'notes_content'
    id = db.Column(db.Integer, primary_key=True)
    internal_note_id = db.Column(db.Integer, db.ForeignKey('internal_notes.id'))
    note_content = db.Column(db.Text())

class SaveNoteSchema(Schema):
    name = fields.Str(
        data_key="noteName",
        required=False,
        validate=[
            validate.Length(
                min=0, max=50,
                error="Note's name must have less than 50 characters."
            ),
            validate.Regexp(
                regex="^[a-zA-Z0-9-_ ]*$",
                error="Notes name must consist of alphanumeric characters or '-'",
            ),
        ],
    )
    note_id = fields.Str(
        data_key="noteId",
        required=True,
    )
    description = fields.Str(
        data_key="noteDescription",
        validate=[
            validate.Length(
                min=0, max=5000,
                error="Note's description must have less than 5000 characters."
            ),
        ],
        required=False,
    )
    user = fields.Str()


class RetrieveNoteSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    note_id = fields.Str()
    type = fields.Int()
    visibility = fields.Int()
    description = fields.Str()
    registered_on = fields.DateTime()
    size = fields.Float()
    note_content = fields.Str()


class RetrieveNoteToEditSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    note_id = fields.Str()
    type = fields.Int()
    visibility = fields.Int()
    description = fields.Str()
    registered_on = fields.DateTime()
    size = fields.Float()

retrieve_note_schema = RetrieveNoteSchema()
retrieve_note_to_edit_schema = RetrieveNoteToEditSchema()

def db_save_note(internal_note_data):
    db.session.add(internal_note_data)
    db.session.flush()
    save_note_primary_key = internal_note_data.id
    db.session.commit()
    db.session.close()
    return save_note_primary_key

def db_edit_note(internal_note_data, note_internal_id):
    db.session.query(InternalNotes).filter(InternalNotes.id == note_internal_id).update(internal_note_data)
    db.session.commit()
    db.session.close()

def db_get_all_notes(user):
    # notes = db.session.query(InternalNotes).filter_by(owner=user).all()
    notes = db.session.query(InternalNotes).filter(InternalNotes.owner == user).all()
    db.session.close()
    return notes

def aaaaaaaa(user):
    # notes = db.session.query(InternalNotes).filter_by(owner=user).all()
    notes = db.session.query(InternalNotes).filter(InternalNotes.owner == user).all()
    db.session.close()
    return notes

def db_get_note(note_internal_id, user):
    note = db.session.query(InternalNotes, NotesContent).filter(InternalNotes.owner == user).join(NotesContent, InternalNotes.id==NotesContent.internal_note_id).filter(InternalNotes.id == note_internal_id).one()
    # soup = BeautifulSoup(note[1].note_content, features="html.parser")
    # html_filtered = soup.find('div', id="doc").string.extract()
    db.session.close()
    return note

def db_get_note_to_edit(note_internal_id, user):
    note = db.session.query(InternalNotes).filter(InternalNotes.owner == user, InternalNotes.id == note_internal_id).all()
    db.session.close()
    return note

def db_remove_note(note_internal_id, user):
    note_row = InternalNotes.query.filter_by(id=note_internal_id).filter(InternalNotes.owner == user).one()
    db.session.delete(note_row)
    db.session.commit()
    db.session.close()
