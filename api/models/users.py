from .utils import db

class LoginUser(db.Model):
    __tablename__ = 'login_user'
    login = db.Column(db.String(100), primary_key=True)
    password = db.Column(db.String(100))


def db_save_user(user):
    db.session.add(user)
    db.session.commit()
    db.session.close()

def login_user(login, password):
    user = LoginUser.query.filter_by(login=login, password=password).all()
    db.session.close()
    return user
