class GetErrorException(Exception):
  """Error thrown during a GET operation"""
  pass

class PostErrorException(Exception):
  """Error thrown during a POST operation"""
  pass

class PutErrorException(Exception):
  """Error thrown during a PUT operation"""
  pass

class DeleteErrorException(Exception):
  """Error thrown during a DELETE operation"""
  pass

class UserErrorException(Exception):
  """Error thrown during a login operation"""
  pass

class InternalErrorException(Exception):
  """Internal error"""
  pass