from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify
from flask_cors import CORS
from views.notes.notes import notes
from views.auth.auth import auth
from exceptions import PostErrorException, DeleteErrorException, PutErrorException, GetErrorException

api = Flask(__name__)

# Endpoints related
CORS(api)
api.register_blueprint(notes)
api.register_blueprint(auth)

# DB related
api.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://biblioteka:biblioteka-pwd@biblioteka-postgresql/biblioteka"
api.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(api)

if __name__ == "__main__":
    api.run()

@api.errorhandler(PostErrorException)
def _handle_post_exception(exc):
    api.logger.error("There was a POST Exception: %s",
        exc, exc_info=True)
    return jsonify(error=str(exc)), 404

@api.errorhandler(DeleteErrorException)
def _handle_delete_exception(exc):
    api.logger.error("There was a DELETE Exception: %s",
        exc, exc_info=True)
    return jsonify(error=str(exc)), 404

@api.errorhandler(PutErrorException)
def _handle_put_exception(exc):
    api.logger.error("There was a PUT Exception: %s",
        exc, exc_info=True)
    return jsonify(error=str(exc)), 404

@api.errorhandler(GetErrorException)
def _handle_get_exception(exc):
    api.logger.error("There was a GET Exception: %s",
        exc, exc_info=True)
    return jsonify(error=str(exc)), 404

@api.errorhandler(Exception)
def _handle_generic_exception(exc):
    api.logger.error("There was an Exception: %s",
        exc, exc_info=True)
    return jsonify(error=str(exc)), 500