import os
import requests
from dotenv import load_dotenv
from typing import Dict
from flask import Blueprint, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from webargs.flaskparser import use_args
from datetime import datetime
from models.notes import SaveNoteSchema, InternalNotes, NotesContent, db_get_note_to_edit, db_save_note, db_get_all_notes, retrieve_note_schema, retrieve_note_to_edit_schema, db_get_note, db_remove_note, db_edit_note
from exceptions import PostErrorException, DeleteErrorException, PutErrorException, GetErrorException

load_dotenv()
db = SQLAlchemy()

notes = Blueprint("notes", __name__, url_prefix="/notes")

@notes.route("/save", methods=["POST"])
@use_args(SaveNoteSchema())
def save_note(note: Dict):
    response = requests.get(os.environ.get("CODIMD_BASE_HOSTNAME") + note["note_id"] + "/download")
    if (response.status_code != 200 or (response.status_code == 200 and response.history)):
        # To hide if the note exists or is just private
        raise PostErrorException("There was a problem saving your note")
    note_md = response.text
    note_content = NotesContent(
        note_content = note_md
    )
    internal_note_data = InternalNotes(
        name=note.get("name", "name of the note"),
        note_id=note["note_id"],
        type = 1,
        visibility = 1,
        description = note.get("description", None),
        registered_on=datetime.now(),
        size = len(response.content)/1024,
        content = note_content,
        owner = note["user"],
    )
    save_note_primary_key = db_save_note(internal_note_data)
    return jsonify(
        message="The note was saved correctly. You will be redirected to it in a few seconds",
        note_id=save_note_primary_key
    ), 200

@notes.route("/edit/<note_internal_id>", methods=["PUT"])
@use_args(SaveNoteSchema(partial=True))
def edit_note(note: Dict, note_internal_id):
    try:
        db_edit_note(note, note_internal_id)
    except Exception as ex:
        print("Error editing note", ex)
        raise PutErrorException("An error occurred while editing the note")
    return jsonify(
        message="The note was edited correctly",
    ), 200

@notes.route("/all", methods=["GET"])
def get_all_notes():
    user = request.args.get("user")
    try:
        all_notes = db_get_all_notes(user)
    except Exception as ex:
        print("Error fetching notes", ex)
        raise GetErrorException("An error occurred while fetching the notes")
    return jsonify(
        notes=retrieve_note_schema.dump(all_notes, many=True)
    ), 200


@notes.route("/note/<note_internal_id>", methods=["GET"])
def get_note(note_internal_id):
    user = request.args.get("user")
    try:
        note = db_get_note(note_internal_id, user)
    except Exception as ex:
        print("Error fetching notes", ex)
        raise GetErrorException("An error occurred while fetching the note")
    return jsonify(note=retrieve_note_schema.dump(note, many=True)), 200


@notes.route("/note/<note_internal_id>/edit", methods=["GET"])
def get_note_to_edit(note_internal_id):
    user = request.args.get("user")
    try:
        note = db_get_note_to_edit(note_internal_id, user)
    except Exception as ex:
        print("Error fetching notes", ex)
        raise GetErrorException("An error occurred while fetching the note")
    if (note):
        return jsonify(note=retrieve_note_to_edit_schema.dump(note, many=True)), 200
    else:
        raise GetErrorException("You do not have privileges to edit this note")


@notes.route("/note/<note_internal_id>", methods=["DELETE"])
def delete_note(note_internal_id):
    user = request.args.get("user")
    try:
        db_remove_note(note_internal_id, user)
    except Exception as ex:
        print("Error removing note", ex)
        raise DeleteErrorException("An error occurred while deleting the note")
    return jsonify(message="The note was removed correctly. You will be redirected to the list of notes"), 200
