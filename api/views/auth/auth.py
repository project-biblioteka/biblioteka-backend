from flask import Blueprint, jsonify, request
from models.users import db_save_user, LoginUser, login_user
from exceptions import UserErrorException

auth = Blueprint("auth", __name__, url_prefix="/auth")

@auth.route('/login', methods=["POST"])
def login():
  user = request.get_json()
  logged = login_user(user["login"], user["password"])
  if not logged:
    print("User incorrect or does not exist")
    raise UserErrorException("The user is not correct or does not exist")
  return jsonify(message="You are logged", user=user["login"]), 200


@auth.route('/signup', methods=["POST"])
def signup():
  user = request.get_json()
  login_user = LoginUser(
    login=user["login"],
    password=user["password"],
  )
  db_save_user(login_user)
  return jsonify(message="You are registered", user=user["login"]), 200
