FROM python:3.9.4-buster

RUN apt-get update && apt-get install -y --no-install-recommends
COPY Pipfile Pipfile.lock ./docker-scripts/entrypoint.sh gunicorn_config.py .flaskenv .env ./
COPY ./api ./api
RUN pip install pipenv && pipenv install --system --deploy --ignore-pipfile && chmod -R 777 entrypoint.sh
EXPOSE 8080

WORKDIR /api
ENTRYPOINT ["../entrypoint.sh"]
#CMD ["/bin/bash", "-c", "while true; do sleep 30; done;"]
