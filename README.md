# Biblioteka backend

This repository contains the frontend implementation in React for the project Biblioteka.


## Biblioteka
Project for the development of a tool for the elaboration of documentation in Markdown language and for archiving it.
This project is divided into two main subprojects, one is a fork from [CodiMD](https://github.com/hackmdio/codimd) and this current subproject, to develop a tool for archiving purpose.


## Run the server
- Activate venv
    ```bash
    . venv/bin/activate
    ```
- Deploy server
    ```bash
    flask run
    ```
- Deactivate venv
    ```bash
    deactivate
    ```

## How to add dependencies to Pipfile

https://stackoverflow.com/questions/46330327/how-are-pipfile-and-pipfile-lock-used

```bash
pipenv install python-dotenv
```

It could be necessary to rebuild the image once `pipenv install pkg`

**Notice that PostgreSQL local DB is forwarded to port 5433 to avoid conflicts with possible PostgreSQL local instances**

It is necessary to manually set the timezone for the DB:
```
SET TIMEZONE TO 'Europe/Madrid';
```
## License

**License under GNU General Public License version 3.**
